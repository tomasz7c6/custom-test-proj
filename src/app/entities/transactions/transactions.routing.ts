import { RouterModule, Routes } from '@angular/router';
import { TransactionsComponent } from "./transactions.component";

const routesConfig: Routes = [
    {
        path: 't',
        component: TransactionsComponent,
    }
]

export const routerModule = RouterModule.forChild(routesConfig)