import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TransactionsComponent } from "./transactions.component";
import { routerModule } from "./transactions.routing";

@NgModule({
  imports: [
    CommonModule,
    routerModule
  ],
  declarations: [
    TransactionsComponent
  ],
  exports: [
    TransactionsComponent
  ]
})
export class TransactionsModule { }
