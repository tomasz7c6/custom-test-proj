import { RouterModule, Routes } from '@angular/router';

const routesConfig: Routes = [
    { path: '', redirectTo: 't', pathMatch: 'full' },
    { path: '**', redirectTo: 't', pathMatch: 'full' }
]

export const routerModule = RouterModule.forRoot(routesConfig, {
    // enableTracing: true,
    // useHash: false
})