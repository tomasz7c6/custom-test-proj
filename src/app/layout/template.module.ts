import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TemplateComponent } from "./template.component";
import { NavbarComponent } from "./navbar/navbar.component";
// import { routerModule } from "./template.routing";

@NgModule({
  imports: [
    CommonModule,
    // routerModule
  ],
  declarations: [
    TemplateComponent,
    NavbarComponent    
  ],
  exports: [
    TemplateComponent
  ]
})
export class TemplateModule { }
