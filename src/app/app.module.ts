import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
// import { TransactionsComponent } from './entities/transactions/transactions.component';
// import { NavbarComponent } from './layout/navbar/navbar.component';
import { routerModule } from './app.routing';
// import { BreadcrumbComponent } from './layout/breadcrumb/breadcrumb.component';
// import { TemplateComponent } from './layout/template.component';
// import { TemplateModule } from "./layout/template.module";
import { TransactionsModule } from "./entities/transactions/transactions.module";
// import { ReportDetailsComponent } from './entities/report-details/report-details.component';


@NgModule({
  declarations: [
    AppComponent,
    // TransactionsComponent,
    // NavbarComponent,
    // BreadcrumbComponent,
    // ReportDetailsComponent,
    // TemplateComponent,
  ],
  imports: [
    TransactionsModule,
    BrowserModule,
    routerModule,
    // TemplateModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
